package com.jonkryl.sekveniatest;

import com.jonkryl.sekveniatest.model.datasource.FilmDataSource;
import com.jonkryl.sekveniatest.model.datasource.FilmDataSourceImpl;
import com.jonkryl.sekveniatest.model.interactor.FilmInteractor;
import com.jonkryl.sekveniatest.model.interactor.FilmInteractorImpl;
import com.jonkryl.sekveniatest.model.repository.FilmRepository;
import com.jonkryl.sekveniatest.model.repository.FilmRepositoryImpl;
import com.jonkryl.sekveniatest.presenter.FilmDetailPresenterImpl;
import com.jonkryl.sekveniatest.presenter.FilmDetailsContract;
import com.jonkryl.sekveniatest.presenter.FilmListContract;
import com.jonkryl.sekveniatest.presenter.FilmListPresenterImpl;
import com.jonkryl.sekveniatest.presenter.Router;

public class InstanceFactory {

    public FilmListContract.Presenter createListPresenter(Router router) {
        FilmDataSource source = new FilmDataSourceImpl();
        FilmRepository repository = new FilmRepositoryImpl(source);
        FilmInteractor interactor = new FilmInteractorImpl(repository);
        return new FilmListPresenterImpl(interactor, router);
    }

    public FilmDetailsContract.Presenter createDetailsPresenter(Router router) {
        return new FilmDetailPresenterImpl(router);
    }
}
