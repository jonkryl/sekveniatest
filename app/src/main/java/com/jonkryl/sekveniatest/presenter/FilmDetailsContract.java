package com.jonkryl.sekveniatest.presenter;

import com.jonkryl.sekveniatest.model.entity.Film;

public interface FilmDetailsContract {

    interface Presenter {

        void onBackClicked();

        void setView(FilmDetailsContract.View view);

        void onDetailsLoaded(Film film);
    }

    interface View {

        void showDetails(Film film);
    }
}
