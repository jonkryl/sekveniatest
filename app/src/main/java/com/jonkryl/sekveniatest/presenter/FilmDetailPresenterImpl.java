package com.jonkryl.sekveniatest.presenter;

import com.jonkryl.sekveniatest.model.entity.Film;

public class FilmDetailPresenterImpl implements FilmDetailsContract.Presenter {

    private FilmDetailsContract.View view;

    private final Router router;

    public FilmDetailPresenterImpl(Router router) {
        this.router = router;
    }

    public void setView(FilmDetailsContract.View view) {
        this.view = view;
    }

    public void onDetailsLoaded(Film film) {
        view.showDetails(film);
    }

    public void onBackClicked() {
        router.openFilmList();
    }
}
