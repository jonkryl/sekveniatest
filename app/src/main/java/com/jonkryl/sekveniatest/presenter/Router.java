package com.jonkryl.sekveniatest.presenter;

import com.jonkryl.sekveniatest.model.entity.Film;

public interface Router {
    void openFilmList();
    void openFilmInfo(Film film);
}
