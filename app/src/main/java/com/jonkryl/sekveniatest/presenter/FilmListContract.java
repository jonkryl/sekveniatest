package com.jonkryl.sekveniatest.presenter;

import com.jonkryl.sekveniatest.model.entity.Film;

import java.util.List;

public interface FilmListContract {

    interface Presenter{
        void setView(View view);

        void onGenreClicked(String item);

        void onFilmClicked(Film item);
    }

    interface View{
        void showList(List<String> data);

        void showImage(List<Film> data);
    }
}
