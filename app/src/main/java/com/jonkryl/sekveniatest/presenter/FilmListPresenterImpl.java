package com.jonkryl.sekveniatest.presenter;

import com.jonkryl.sekveniatest.model.Callback;
import com.jonkryl.sekveniatest.model.entity.Film;
import com.jonkryl.sekveniatest.model.interactor.FilmInteractor;

import java.util.List;

public class FilmListPresenterImpl implements FilmListContract.Presenter {

    private final FilmInteractor filmInteractor;

    private FilmListContract.View view;
    private final Router router;

    public FilmListPresenterImpl(FilmInteractor filmInteractor, Router router) {
        this.router = router;
        this.filmInteractor = filmInteractor;
    }

    public void setView(FilmListContract.View view) {
        this.view = view;
        if (this.view != null) {
            start();
        }
    }

    private void start() {
        getListFilms();
        getImageFilms();
    }

    private void getListFilms() {
        filmInteractor.loadGenres(new Callback<List<String>>() {
            @Override
            public void okLoad(List<String> data) {
                view.showList(data);
            }
        });
    }

    private void getImageFilms() {
        filmInteractor.loadFilm(new Callback<List<Film>>() {
            @Override
            public void okLoad(List<Film> data) {
                view.showImage(data);
            }
        });
    }

    public void onGenreClicked(final String name) {
        filmInteractor.getFilteredFilms(name, new Callback<List<Film>>() {
            @Override
            public void okLoad(List<Film> data) {
                view.showImage(data);
            }
        });
    }

    public void onFilmClicked(Film film) {
        router.openFilmInfo(film);
    }

}
