package com.jonkryl.sekveniatest.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jonkryl.sekveniatest.R;
import com.jonkryl.sekveniatest.model.entity.Film;
import com.jonkryl.sekveniatest.presenter.Router;
import com.jonkryl.sekveniatest.view.fragments.FilmListFragment;
import com.jonkryl.sekveniatest.view.fragments.FilmDetailsFragment;

public class MainActivity extends AppCompatActivity implements Router {

    private FilmListFragment fragmentList;
    private FilmDetailsFragment fragmentMore;

    private final String FILM = "FILM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentList = new FilmListFragment();
        fragmentMore = new FilmDetailsFragment();
        openFilmList();
    }

    @Override
    public void openFilmList() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, fragmentList)
                .commit();
    }

    @Override
    public void openFilmInfo(Film film) {
        Bundle args = new Bundle();
        args.putSerializable(FILM, film);
        fragmentMore.setArguments(args);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, fragmentMore)
                .commit();
    }

    @Override
    public void onBackPressed() {
        openFilmList();
    }
}
