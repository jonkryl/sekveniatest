package com.jonkryl.sekveniatest.view.fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jonkryl.sekveniatest.R;
import com.jonkryl.sekveniatest.model.entity.Film;
import com.squareup.picasso.Picasso;

import java.util.List;

class CustomGridViewAdapter extends BaseAdapter {
    private final Context mContext;
    private final List<Film> filmImages;
    private final int SIZE = 200;

    public CustomGridViewAdapter(Context c, List<Film> filmImages) {
        mContext = c;
        this.filmImages = filmImages;
    }

    @Override
    public int getCount() {
        return filmImages.size();
    }

    @Override
    public Film getItem(int position) {
        return filmImages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;
        ItemHolder itemHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.item_image_layout, null);

            itemHolder = new ItemHolder();
            itemHolder.title = grid.findViewById(R.id.tvOverlayText);
            itemHolder.image = grid.findViewById(R.id.ivBackground);

            grid.setTag(itemHolder);
        } else {
            grid = convertView;
            itemHolder = (ItemHolder) grid.getTag();
        }

        itemHolder.title.setText(filmImages.get(position).getLocalized_name());
        Picasso.get()
                .load(filmImages.get(position).getImage_url())
                .placeholder(R.drawable.ic_block_black_200dp)
                .resize(SIZE, SIZE)
                .into(itemHolder.image);

        return grid;
    }

    private class ItemHolder {
        private TextView title;
        private ImageView image;
    }
}
