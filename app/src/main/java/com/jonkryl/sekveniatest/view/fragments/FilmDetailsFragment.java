package com.jonkryl.sekveniatest.view.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jonkryl.sekveniatest.InstanceFactory;
import com.jonkryl.sekveniatest.R;
import com.jonkryl.sekveniatest.model.entity.Film;
import com.jonkryl.sekveniatest.presenter.FilmDetailsContract;
import com.jonkryl.sekveniatest.presenter.Router;
import com.squareup.picasso.Picasso;

public class FilmDetailsFragment extends Fragment implements FilmDetailsContract.View {

    private ImageView imageView;
    private TextView name_england;
    private TextView year;
    private TextView rating;
    private TextView information_more;

    private FilmDetailsContract.Presenter presenter;

    private final InstanceFactory factory = new InstanceFactory();

    private final String FILM = "FILM";

    private final int FOUR = 4;
    private final int SEVEN = 7;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.info_layout, container, false);
        addActionBar();
        return v;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            presenter.onBackClicked();
            return true;
        }
        return false;
    }

    private void initResource() {
        presenter = factory.createDetailsPresenter((Router) getActivity());

        imageView = getActivity().findViewById(R.id.image_more);
        name_england = getActivity().findViewById(R.id.name_england);
        year = getActivity().findViewById(R.id.year);
        rating = getActivity().findViewById(R.id.rating);
        information_more = getActivity().findViewById(R.id.information_more);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initResource();
        presenter.setView(this);
        presenter.onDetailsLoaded((Film) getArguments().getSerializable(FILM));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.setView(null);
        presenter = null;
    }

    @Override
    public void showDetails(Film film) {
        if (film != null) {
            Picasso.get()
                    .load(film.getImage_url())
                    .placeholder(R.drawable.ic_block_black_200dp)
                    .into(imageView);
            name_england.setText(film.getName());
            year.setText("Год: " + film.getYear());
            rating.setText(String.valueOf(film.getRating()));
            information_more.setText(film.getDescription());
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(film.getLocalized_name());
            setColorRating(film.getRating());
        }
    }

    private void setColorRating(float rating) {
        if (rating < FOUR) {
            colorRating(Color.RED);
        }
        if (FOUR < rating && rating < SEVEN) {
            colorRating(Color.YELLOW);
        }
        if (rating > SEVEN) {
            colorRating(Color.GREEN);
        }
    }

    private void colorRating(int color) {
        rating.setTextColor(color);
    }

    private void addActionBar() {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        setHasOptionsMenu(true);
    }
}
