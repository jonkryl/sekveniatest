package com.jonkryl.sekveniatest.view.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;

import com.jonkryl.sekveniatest.InstanceFactory;
import com.jonkryl.sekveniatest.R;
import com.jonkryl.sekveniatest.model.entity.Film;
import com.jonkryl.sekveniatest.presenter.FilmListContract;
import com.jonkryl.sekveniatest.presenter.Router;

import java.util.List;

public class FilmListFragment extends Fragment implements FilmListContract.View {

    private FilmListContract.Presenter presenter;

    private ListView listView;
    private GridView gridView;

    private ArrayAdapter<String> adapterGenres;
    private CustomGridViewAdapter customGridViewAdapter;

    private final InstanceFactory factory = new InstanceFactory();

    private int save;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = factory.createListPresenter((Router) getActivity());
        presenter.setView(this);

        listView = getActivity().findViewById(R.id.listGenres);
        gridView = getActivity().findViewById(R.id.listFilms);
    }

    @Override
    public void showList(List<String> list) {
        if (adapterGenres != null) {
            adapterGenres = null;
        }
        adapterGenres = new ArrayAdapter<>(getActivity(),
                R.layout.item_genres_layout, list);
        listView.setAdapter(adapterGenres);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                presenter.onGenreClicked(adapterGenres.getItem(position));

                parent.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.stranyi));
                if (save != -1 && save != position) {
                    parent.getChildAt(save).setBackgroundColor(getResources().getColor(R.color.white));
                }
                save = position;
            }
        });
    }

    @Override
    public void showImage(List<Film> filmImages) {
        if (customGridViewAdapter != null) {
            customGridViewAdapter = null;
        }
        customGridViewAdapter = new CustomGridViewAdapter(getActivity(), filmImages);
        gridView.setAdapter(customGridViewAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                presenter.onFilmClicked(customGridViewAdapter.getItem(position));
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        addActionBar();
        return inflater.inflate(R.layout.list_layout, null);
    }

    private void addActionBar() {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setTitle(getResources().getString(R.string.films));
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.setView(null);
        presenter = null;
    }
}
