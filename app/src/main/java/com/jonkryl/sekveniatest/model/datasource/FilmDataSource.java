package com.jonkryl.sekveniatest.model.datasource;

import com.jonkryl.sekveniatest.model.entity.FilmWrapper;

import retrofit2.Call;

public interface FilmDataSource {
    Call<FilmWrapper> getFilms();
}
