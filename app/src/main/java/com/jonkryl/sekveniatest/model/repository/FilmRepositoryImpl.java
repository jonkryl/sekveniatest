package com.jonkryl.sekveniatest.model.repository;

import com.jonkryl.sekveniatest.model.Callback;
import com.jonkryl.sekveniatest.model.datasource.FilmDataSource;
import com.jonkryl.sekveniatest.model.entity.Film;
import com.jonkryl.sekveniatest.model.entity.FilmWrapper;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class FilmRepositoryImpl implements FilmRepository {

    private List<Film> filmList;

    private final FilmDataSource dataSource;

    public FilmRepositoryImpl(FilmDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void getFilms(final Callback<List<Film>> callback) {
        if (filmList == null) {
            dataSource.getFilms().enqueue(new retrofit2.Callback<FilmWrapper>() {
                @Override
                public void onResponse(Call<FilmWrapper> call, Response<FilmWrapper> response) {
                    List<Film> films = response.body().getFilms();
                    Collections.sort(films);
                    filmList = films;
                    callback.okLoad(filmList);
                }

                @Override
                public void onFailure(Call<FilmWrapper> call, Throwable t) {
                }
            });
        } else {
            callback.okLoad(filmList);
        }
    }
}
