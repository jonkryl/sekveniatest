package com.jonkryl.sekveniatest.model.interactor;

import android.support.v4.util.ArraySet;

import com.jonkryl.sekveniatest.model.Callback;
import com.jonkryl.sekveniatest.model.entity.Film;
import com.jonkryl.sekveniatest.model.repository.FilmRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class FilmInteractorImpl implements FilmInteractor {

    private final FilmRepository repository;

    public FilmInteractorImpl(FilmRepository repository) {
        this.repository = repository;
    }

    @Override
    public void loadFilm(final Callback<List<Film>> callback) {
        repository.getFilms(new Callback<List<Film>>() {
            @Override
            public void okLoad(List<Film> films) {
                Collections.sort(films);
                callback.okLoad(films);
            }
        });
    }

    @Override
    public void loadGenres(final Callback<List<String>> callback) {
        loadFilm(new Callback<List<Film>>() {
            @Override
            public void okLoad(List<Film> films) {
                Set<String> genres = new ArraySet<>();
                for (int i = 0; i < films.size(); i++) {
                    genres.addAll(films.get(i).getGenres());
                }
                List<String> genreList = new ArrayList<>(genres);
                callback.okLoad(genreList);
            }
        });
    }

    @Override
    public void getFilteredFilms(final String filter, final Callback<List<Film>> callback) {
        loadFilm(new Callback<List<Film>>() {
            @Override
            public void okLoad(List<Film> films) {
                final Set<Film> filtrFilms = new ArraySet<>();
                for (int i = 0; i < films.size(); i++) {
                    for (int j = 0; j < films.get(i).getGenres().size(); j++) {
                        if (films.get(i).getGenres().get(j).equals(filter)) {
                            filtrFilms.add(films.get(i));
                        }
                    }
                }
                final List<Film> filteredList = new ArrayList<>(filtrFilms);
                callback.okLoad(filteredList);
            }
        });
    }
}
