package com.jonkryl.sekveniatest.model.interactor;

import com.jonkryl.sekveniatest.model.Callback;
import com.jonkryl.sekveniatest.model.entity.Film;

import java.util.List;

public interface FilmInteractor {
    void loadGenres(Callback<List<String>> listCallback);

    void loadFilm(Callback<List<Film>> listCallback);

    void getFilteredFilms(String name, Callback<List<Film>> listCallback);
}
