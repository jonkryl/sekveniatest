package com.jonkryl.sekveniatest.model.datasource;


import com.jonkryl.sekveniatest.model.entity.FilmWrapper;
import com.jonkryl.sekveniatest.model.network.APIService;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FilmDataSourceImpl implements FilmDataSource {

    private APIService apiService;
    private final String BASE_URL = "https://s3-eu-west-1.amazonaws.com/";

    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    public Call<FilmWrapper> getFilms() {
        return getApiService().getFilms();
    }

    private APIService getApiService() {
        if (apiService == null) {
            apiService = retrofit.create(APIService.class);
        }
        return apiService;
    }
}
