package com.jonkryl.sekveniatest.model.repository;

import com.jonkryl.sekveniatest.model.Callback;
import com.jonkryl.sekveniatest.model.entity.Film;

import java.util.List;

public interface FilmRepository {
    void getFilms(Callback<List<Film>> listCallback);
}
