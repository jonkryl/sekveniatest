package com.jonkryl.sekveniatest.model;

public interface Callback<T> {
    void okLoad(T data);
}
