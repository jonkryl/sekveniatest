package com.jonkryl.sekveniatest.model.entity;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

public class Film implements Comparable<Film>, Serializable {
    private int id, year;
    private float rating;
    private String localized_name, name, image_url, description;
    private ArrayList<String> genres;

    public float getRating() {
        return rating;
    }

    public int getId() {
        return id;
    }

    public int getYear() {
        return year;
    }

    public String getLocalized_name() {
        return localized_name;
    }

    public String getName() {
        return name;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<String> getGenres() {
        return genres;
    }

    @Override
    public int compareTo(@NonNull Film o) {
        return this.localized_name.compareTo(o.localized_name);
    }
}
