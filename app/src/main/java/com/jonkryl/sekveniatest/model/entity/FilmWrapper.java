package com.jonkryl.sekveniatest.model.entity;

import java.util.List;

public class FilmWrapper {
    private List<Film> films;

    public List<Film> getFilms() {
        return films;
    }
}
