package com.jonkryl.sekveniatest.model.network;

import com.jonkryl.sekveniatest.model.entity.FilmWrapper;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIService {
        @GET("sequeniatesttask/films.json")
        Call<FilmWrapper> getFilms();
}
